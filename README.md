:warning: This project is currently at very early stage and not functional. :warning:

# SugarTable

Build your iOS tables easily with sugarfull syntax.

## Introduction

**SugarTable** is a library for building iOS tables.
It provides sugarfull syntax and powerfull support for implementing your custom `UITableView`s.

## Install

TODO

## Usage

TODO

## LICENSE

SugarTable is available under the MIT license.
