//
//  AnyBinding.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/03/03.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import enum Result.NoError
import ReactiveSwift

final class AnySectionBinding {

    init(
        type: HeaderFooterType,
        condition: @escaping (Any) -> Bool,
        title: ((Any, Int, UITableView) -> String?)?,
        view: ((Any, Int, UITableView) -> UITableViewHeaderFooterView?)?,
        height: @escaping (Any, Int, UITableView) -> CGFloat,
        estimatedHeight: @escaping (Any, Int, UITableView) -> CGFloat
    ) {
        self.type = type
        self.condition = condition
        self.title = title
        self.view = view
        self.height = height
        self.estimatedHeight = estimatedHeight
    }

    let type: HeaderFooterType
    let condition: (Any) -> Bool
    let title: ((Any, Int, UITableView) -> String?)?
    let view: ((Any, Int, UITableView) -> UITableViewHeaderFooterView?)?
    let height: (Any, Int, UITableView) -> CGFloat
    let estimatedHeight: (Any, Int, UITableView) -> CGFloat

}

final class AnyRowBinding {

    init(cellID: String) {
        self.cellID = cellID
    }

    let cellID: String
    var cell: (Any, IndexPath, UITableView) -> UITableViewCell = { _ in fatalError() }
    var height: (Any, IndexPath, UITableView) -> CGFloat = { _ in fatalError() }
    var estimatedHeight: (Any, IndexPath, UITableView) -> CGFloat = { _ in fatalError() }
    var editing: (Any, IndexPath, UITableView) -> EditingStyle = { _ in fatalError() }
    var condition: (Any, IndexPath) -> Bool = { _ in fatalError() }

    let (didSelect, didSelectObserver) = Signal<(UITableViewCell?, Any, IndexPath, UITableView), NoError>.pipe()
    let (didDeselect, didDeselectObserver) = Signal<(UITableViewCell?, Any, IndexPath, UITableView), NoError>.pipe()
    let (commit, commitObserver) = Signal<(Any, UITableViewCellEditingStyle, IndexPath, UITableView), NoError>.pipe()

}
