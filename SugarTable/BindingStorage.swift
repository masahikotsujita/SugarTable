//
//  BindingStorage.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/05/03.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

protocol BindingStorageProtocol {
    func bindingForRow(at indexPath: IndexPath, context: Any) -> AnyRowBinding
}
