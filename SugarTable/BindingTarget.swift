//
//  BindingTarget.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import enum Result.NoError
import ReactiveSwift

public final class TableBindingTarget: BindingStorageProtocol {

    private var sectionBindings = [AnySectionBinding]()

    func sectionBinding(of type: HeaderFooterType, for model: Any) -> AnySectionBinding? {
        return self.sectionBindings
            .filter { $0.condition(model) && $0.type == type }
            .first
    }

    private var rowBindings = [AnyRowBinding]()

    func bindingForRow(at indexPath: IndexPath, context: Any) -> AnyRowBinding {
        return self.rowBindings
            .filter { $0.condition(context, indexPath) }
            .first!
    }

    public func cells<Cell: UITableViewCell>(
        as class: Cell.Type,
        id: String
    ) -> VirtualCellCollectionRepresentation<Cell> {
        let anyBinding = AnyRowBinding(cellID: id)
        let binding = VirtualCellCollectionRepresentation<Cell>(base: anyBinding)
        self.rowBindings.append(anyBinding)
        return binding
    }

}

public final class VirtualCellCollectionRepresentation<Cell: UITableViewCell> {

    init(base: AnyRowBinding) {
        self.base = base
    }

    let base: AnyRowBinding

    @discardableResult
    public func bind<RowModel>(
        with collection: VirtualCollection<(RowModel, IndexPath)>,
        configuration: @escaping (Cell, RowModel, IndexPath, UITableView) -> Void,
        height: RowHeight<RowModel>,
        editing: @escaping (RowModel, IndexPath, UITableView) -> EditingStyle = { _ in .none },
        _ body: (CellBinding<Cell, RowModel>) -> Void = { _ in }
    ) {
        self.base.cell = { model, indexPath, tableView in
            let cell = tableView.dequeueReusableCell(withIdentifier: self.base.cellID, for: indexPath) as! Cell
            configuration(cell, model as! RowModel, indexPath, tableView)
            return cell
        }
        self.base.height = { height.heightFunc($0 as! RowModel, $1, $2) }
        self.base.estimatedHeight = { height.estimatedHeightFunc($0 as! RowModel, $1, $2) }
        self.base.editing = { editing($0 as! RowModel, $1, $2) }
        self.base.condition = { collection.contains(($0 as! RowModel, $1)) }
        let binding = CellBinding<Cell, RowModel>(base: self.base)
        body(binding)
    }

}

public final class CellBinding<Cell: UITableViewCell, RowModel> {

    init(base: AnyRowBinding) {
        self.base = base
    }

    let base: AnyRowBinding

    public var didSelect: Signal<(Cell?, RowModel, IndexPath, UITableView), NoError> {
        return self.base.didSelect.map { ($0 as? Cell, $1 as! RowModel, $2, $3) }
    }

    public var didDeselect: Signal<(Cell?, RowModel, IndexPath, UITableView), NoError> {
        return self.base.didDeselect.map { ($0 as? Cell, $1 as! RowModel, $2, $3) }
    }

    public var commit: Signal<(RowModel, UITableViewCellEditingStyle, IndexPath, UITableView), NoError> {
        return self.base.commit.map { ($0 as! RowModel, $1, $2, $3) }
    }

}
