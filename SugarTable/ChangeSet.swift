//
//  ChangeSet.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

public enum SectionChange {
    case inserted(Int)
    case removed(Int)
    case updated(Int)
}

public enum RowChange {
    case inserted(IndexPath)
    case removed(IndexPath)
    case updated(IndexPath)
}

public enum PartialChange {
    case section(SectionChange)
    case row(RowChange)
}

public enum ChangeSet {
    case whole
    case partial([PartialChange])
}
