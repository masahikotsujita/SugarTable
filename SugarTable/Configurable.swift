//
//  Configurable.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

public protocol Configurable {
    associatedtype Model
    func configure(with model: Model)
}
