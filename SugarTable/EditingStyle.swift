//
//  EditingStyle.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/03/03.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit

public enum EditingStyle: Equatable {
    case none
    case delete
    case insert
    case custom([UITableViewRowAction])
}

extension UITableViewCellEditingStyle {
    init(_ editingStyle: EditingStyle) {
        switch editingStyle {
        case .none:
            self = .none
        case .insert:
            self = .insert
        case .delete, .custom:
            self = .delete
        }
    }
}

public func == (lhs: EditingStyle, rhs: EditingStyle) -> Bool {
    switch (lhs, rhs) {
    case (.none, .none), (.delete, .delete), (.insert, .insert):
        return true
    case let (.custom(actions1), .custom(actions2)):
        return actions1 == actions2
    default:
        return false
    }
}
