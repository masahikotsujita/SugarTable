//
//  Height.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/03/06.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

public enum RowHeight<RowModel> {
    case automatic(estimated: CGFloat)
    case value(CGFloat)
    case custom((RowModel, IndexPath, UITableView) -> CGFloat, estimated: ((RowModel, IndexPath, UITableView) -> CGFloat)?)
}

public enum HeaderFooterHeight<SectionModel> {
    case automatic(estimated: CGFloat)
    case value(CGFloat)
    case custom((SectionModel, Int, UITableView) -> CGFloat, estimated: ((SectionModel, Int, UITableView) -> CGFloat)?)
}

public enum TitleHeaderFooterHeight<SectionModel> {
    case automatic
    case value(CGFloat)
    case custom((SectionModel, Int, UITableView) -> CGFloat, estimated: ((SectionModel, Int, UITableView) -> CGFloat)?)
}

internal extension RowHeight {

    var heightFunc: (RowModel, IndexPath, UITableView) -> CGFloat {
        switch self {
        case .automatic:
            return  { _ in UITableViewAutomaticDimension }
        case let .value(height):
            return { _ in height }
        case let .custom(height, estimated: _):
            return height
        }
    }

    var estimatedHeightFunc:  (RowModel, IndexPath, UITableView) -> CGFloat {
        switch self {
        case let .automatic(estimated: estimatedHeight):
            return { _ in estimatedHeight }
        case let .value(height):
            return { _ in height }
        case let .custom(height, estimated: estimatedHeight):
            return estimatedHeight ?? height
        }
    }

}

internal extension TitleHeaderFooterHeight {

    var heightFunc: (SectionModel, Int, UITableView) -> CGFloat {
        switch self {
        case .automatic:
            return { _ in UITableViewAutomaticDimension }
        case let .value(height):
            return { _ in height }
        case let .custom(height, estimated: _):
            return height
        }
    }

    var estimatedHeightFunc: (SectionModel, Int, UITableView) -> CGFloat {
        switch self {
        case .automatic:
            return { _ in 24 }
        case let .value(height):
            return { _ in height }
        case let .custom(height, estimated: estimatedHeight):
            return estimatedHeight ?? height
        }
    }

}

internal extension HeaderFooterHeight {

    var heightFunc: (SectionModel, Int, UITableView) -> CGFloat {
        switch self {
        case .automatic:
            return { _ in UITableViewAutomaticDimension }
        case let .value(height):
            return { _ in height }
        case let .custom(height, estimated: _):
            return height
        }
    }

    var estimatedHeightFunc: (SectionModel, Int, UITableView) -> CGFloat {
        switch self {
        case let .automatic(estimated: estimatedHeight):
            return { _ in estimatedHeight }
        case let .value(height):
            return { _ in height }
        case let .custom(height, estimated: estimatedHeight):
            return estimatedHeight ?? height
        }
    }

}
