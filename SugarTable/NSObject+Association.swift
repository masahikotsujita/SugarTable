//
//  NSObject+Association.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

extension NSObject {

    func getLazyAssociatedValue<Value>(forKey key: StaticString = #function, initial: () -> Value) -> Value {
        var value = objc_getAssociatedObject(self, key.utf8Start) as? Value
        if value == nil {
            value = initial()
            objc_setAssociatedObject(self, key.utf8Start, value!, .OBJC_ASSOCIATION_RETAIN)
        }
        return value!
    }

    func getAssociatedValueContextually<Value>(forKey key: StaticString = #function) -> Value? {
        return objc_getAssociatedObject(self, key.utf8Start) as? Value
    }

    func setAssociatedValueContextually<Value>(_ value: Value, policy: objc_AssociationPolicy, forKey key: StaticString = #function) {
        objc_setAssociatedObject(self, key.utf8Start, value, policy)
    }

}
