//
//  Pipe.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/28.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation
import ReactiveSwift

final class Pipe<Value, Error: Swift.Error> {
    typealias Signal = ReactiveSwift.Signal<Value, Error>
    typealias Observer = ReactiveSwift.Observer<Value, Error>
    let signal: Signal
    let observer: Observer
    init(_ base: (Signal, Observer)) {
        self.signal = base.0
        self.observer = base.1
    }
}
