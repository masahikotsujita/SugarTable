//
// SugarTableExtensions.swift
// SugarTable
//
// Created by Masahiko Tsujita on 2017/05/03.
// Copyright (c) 2017 Masahiko Tsujita. All rights reserved.
//

public final class SugarTableExtensions<Base> {

    init(base: Base) {
        self.base = base
    }

    let base: Base

}

public protocol SugarTableExtensionsProviderProtocol {}

public extension SugarTableExtensionsProviderProtocol {

    public var sugarTable: SugarTableExtensions<Self> {
        return SugarTableExtensions(base: self)
    }

}
