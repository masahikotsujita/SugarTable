//
//  TableDataSource.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/04/25.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation
import enum Result.NoError
import ReactiveSwift

public protocol TableDataSourceProtocol {
    var sectionCount: Int { get }
    func rowCount(ofSectionAt sectionIndex: Int) -> Int
    func context(forRowAt indexPath: IndexPath) -> Any
    func context(forSectionAt sectionIndex: Int) -> Any
    var changes: SignalProducer<ChangeSet, NoError> { get }
}
