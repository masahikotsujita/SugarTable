//
//  TableViewManager.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift

public enum HeaderFooterType {
    case header
    case footer
}

public class TableViewManager: NSObject, UITableViewDataSource, UITableViewDelegate {

    // MARK: - Initializing TableViewBindingHelper

    init(tableModel: TableDataSourceProtocol, bindingProvider: BindingStorageProtocol) {
        self.tableModel = tableModel
        self.bindingStorage = bindingProvider
        super.init()
    }

    let tableModel: TableDataSourceProtocol
    let bindingStorage: BindingStorageProtocol

    // MARK: - UITableViewDataSource

    public func numberOfSections(in tableView: UITableView) -> Int {
        return tableModel.sectionCount
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableModel.rowCount(ofSectionAt: section)
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let context = tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        let cell = binding.cell(context, indexPath, tableView)
        cell.binding = binding
        return cell
    }

    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .header, for: model) else {
//            return nil
//        }
//        return binding.title?(model, section, tableView)
        return nil
    }

    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .footer, for: model) else {
//            return nil
//        }
//        return binding.title?(model, section, tableView)
        return nil
    }

    // MARK: - UITableViewDelegate

    // MARK: Cell

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let context = tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        let height = binding.height(context, indexPath, tableView)
        return height
    }

    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let context = tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        let estimatedHeight = binding.estimatedHeight(context, indexPath, tableView)
        return estimatedHeight
    }

    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let disposable = CompositeDisposable()
        cell.didEndDisplayDisposable = disposable
        cell.willDisplayPipe.observer.send(value: (tableView, indexPath, disposable))
    }

    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.didEndDisplayPipe.observer.send(value: (tableView, indexPath))
        cell.didEndDisplayDisposable?.dispose()
        cell.didEndDisplayDisposable = nil
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let context = self.tableModel.context(forRowAt: indexPath)
        bindingStorage.bindingForRow(at: indexPath, context: context)
            .didSelectObserver.send(value: (cell, context as Any, indexPath, tableView))
    }

    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        let context = self.tableModel.context(forRowAt: indexPath)
        bindingStorage.bindingForRow(at: indexPath, context: context)
            .didDeselectObserver.send(value: (cell, context as Any, indexPath, tableView))
    }

    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let context = self.tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        let editing = binding.editing(context, indexPath, tableView)
        return editing != .none
    }

    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        let context = self.tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        let editing = binding.editing(context, indexPath, tableView)
        return UITableViewCellEditingStyle(editing)
    }

    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let context = self.tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        let editing = binding.editing(context, indexPath, tableView)
        guard case let .custom(actions) = editing else {
            return nil
        }
        return actions
    }

    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let context = self.tableModel.context(forRowAt: indexPath)
        let binding = bindingStorage.bindingForRow(at: indexPath, context: context)
        binding.commitObserver.send(value: (context, editingStyle, indexPath, tableView))
    }

    // MARK: Header

    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .header, for: model) else {
//            return nil
//        }
//        guard let view = binding.view?(model, section, tableView) else {
//            return nil
//        }
//        view.binding = binding
//        return view
        return nil
    }

    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .header, for: model) else {
//            return 0
//        }
//        return binding.height(model, section, tableView)
        return 0
    }

    public func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .header, for: model) else {
//            return 0
//        }
//        return binding.estimatedHeight(model, section, tableView)
        return 0
    }

    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        let disposable = CompositeDisposable()
        headerView.didEndDisplayDisposable = disposable
        headerView.willDisplayPipe.observer.send(value: (tableView, section, disposable))
    }

    public func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.didEndDisplayPipe.observer.send(value: (tableView, section))
        headerView.didEndDisplayDisposable?.dispose()
        headerView.didEndDisplayDisposable = nil
    }

    // MARK: Footer

    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .footer, for: model) else {
//            return nil
//        }
//        guard let view = binding.view?(model, section, tableView) else {
//            return nil
//        }
//        view.binding = binding
//        return view
        return nil
    }

    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .footer, for: model) else {
//            return 0
//        }
//        return binding.height(model, section, tableView)
        return 0
    }

    public func tableView(_ tableView: UITableView, estimatedHeightForFooterInSection section: Int) -> CGFloat {
//        let model = tableModel.object(forSectionAt: section)
//        guard let binding = bindingStorage.sectionBinding(of: .footer, for: model) else {
//            return 0
//        }
//        return binding.estimatedHeight(model, section, tableView)
        return 0
    }

    public func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footerView = view as! UITableViewHeaderFooterView
        let disposable = CompositeDisposable()
        footerView.didEndDisplayDisposable = disposable
        footerView.willDisplayPipe.observer.send(value: (tableView, section, disposable))
    }

    public func tableView(_ tableView: UITableView, didEndDisplayingFooterView view: UIView, forSection section: Int) {
        let footerView = view as! UITableViewHeaderFooterView
        footerView.didEndDisplayPipe.observer.send(value: (tableView, section))
        footerView.didEndDisplayDisposable?.dispose()
        footerView.didEndDisplayDisposable = nil
    }
    
}

private extension UITableViewCell {

    var didEndDisplayDisposable: CompositeDisposable? {
        get {
            return getAssociatedValueContextually()
        }
        set {
            setAssociatedValueContextually(newValue, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var binding: AnyRowBinding? {
        get {
            return getAssociatedValueContextually()
        }
        set {
            setAssociatedValueContextually(newValue, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }

    }
}

private extension UITableViewHeaderFooterView {

    var didEndDisplayDisposable: CompositeDisposable? {
        get {
            return getAssociatedValueContextually()
        }
        set {
            setAssociatedValueContextually(newValue, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    var binding: AnySectionBinding? {
        get {
            return getAssociatedValueContextually()
        }
        set {
            setAssociatedValueContextually(newValue, policy: .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

}
