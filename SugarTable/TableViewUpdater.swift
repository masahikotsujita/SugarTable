//
// TableViewUpdater.swift
// SugarTable
//
// Created by Masahiko Tsujita on 2017/05/03.
// Copyright (c) 2017 Masahiko Tsujita. All rights reserved.
//

import UIKit
import enum Result.NoError
import ReactiveSwift

public class TableViewUpdater {

    init(tableView: UITableView, changes: SignalProducer<ChangeSet, NoError>) {
        self.tableView = tableView
        self.disposable = changes
            .startWithValues { _ in
                self.tableView.reloadData()
            }
    }

    deinit {
        disposable.dispose()
    }

    let tableView: UITableView
    var disposable: Disposable!

}
