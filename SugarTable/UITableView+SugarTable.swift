//
//  UITableView+SugarTable.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/18.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import ReactiveSwift

extension UITableView: SugarTableExtensionsProviderProtocol {}

public extension SugarTableExtensions where Base: UITableView {

    @discardableResult
    public func bind<TableModel:TableDataSourceProtocol>(_ tableModel: TableModel, bindingClosure: (TableBindingTarget, TableModel) -> Void) -> Disposable {
        let tableView: UITableView = self.base
        let tableBindingTarget = TableBindingTarget()
        bindingClosure(tableBindingTarget, tableModel)
        let manager = TableViewManager(tableModel: tableModel, bindingProvider: tableBindingTarget)
        let updater = TableViewUpdater(tableView: self.base, changes: tableModel.changes)
        self.manager = manager
        self.updater = updater
        tableView.dataSource = manager
        tableView.delegate = manager
        tableView.reloadData()
        return ActionDisposable { [weak tableView] in
            guard let `extension` = tableView?.sugarTable else {
                return
            }
            `extension`.manager = nil
            `extension`.updater = nil
        }
    }

    public var manager: TableViewManager? {
        get {
            return base.getAssociatedValueContextually()
        }
        set {
            base.setAssociatedValueContextually(newValue, policy: .OBJC_ASSOCIATION_RETAIN)
        }
    }

    public var updater: TableViewUpdater? {
        get {
            return base.getAssociatedValueContextually()
        }
        set {
            base.setAssociatedValueContextually(newValue, policy: .OBJC_ASSOCIATION_RETAIN)
        }
    }

}
