//
//  UITableViewCell+SugarTable.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import enum Result.NoError
import ReactiveSwift

extension UITableViewCell: SugarTableExtensionsProviderProtocol {}

extension UITableViewCell {

    internal var willDisplayPipe: Pipe<(UITableView, IndexPath, CompositeDisposable), NoError> {
        return getLazyAssociatedValue(initial: {
            return Pipe(Signal.pipe())
        })
    }

    internal var didEndDisplayPipe: Pipe<(UITableView, IndexPath), NoError> {
        return getLazyAssociatedValue(initial: {
            return Pipe(Signal.pipe())
        })
    }

}

public extension SugarTableExtensions where Base: UITableViewCell {

    public var willDisplay: Signal<(UITableView, IndexPath, CompositeDisposable), NoError> {
        return self.base.willDisplayPipe.signal
    }

    public var didEndDisplay: Signal<(UITableView, IndexPath), NoError> {
        return self.base.didEndDisplayPipe.signal
    }

}
