//
//  UITableViewHeaderFooterView+SugarTable.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import enum Result.NoError
import ReactiveSwift

extension UITableViewHeaderFooterView: SugarTableExtensionsProviderProtocol {}

internal extension UITableViewHeaderFooterView {

    internal var willDisplayPipe: Pipe<(UITableView, Int, CompositeDisposable), NoError> {
        return getLazyAssociatedValue(initial: {
            return Pipe(Signal.pipe())
        })
    }

    internal var didEndDisplayPipe: Pipe<(UITableView, Int), NoError> {
        return getLazyAssociatedValue(initial: {
            return Pipe(Signal.pipe())
        })
    }

}

public extension SugarTableExtensions where Base: UITableViewHeaderFooterView {

    public var willDisplay: Signal<(UITableView, Int, CompositeDisposable), NoError> {
        return self.base.willDisplayPipe.signal
    }

    public var didEndDisplay: Signal<(UITableView, Int), NoError> {
        return self.base.didEndDisplayPipe.signal
    }

}
