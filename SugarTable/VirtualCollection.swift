//
//  VirtualCollection.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/05/04.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

public class VirtualCollection<Element> {

    public init(_ contains: @escaping (Element) -> Bool = { _ in true }) {
        self.containsClosure = contains
    }

    private let containsClosure: (Element) -> Bool

    public func contains(_ element: Element) -> Bool {
        return containsClosure(element)
    }

    public func filter(_ contains: @escaping (Element) -> Bool) -> VirtualCollection<Element> {
        return VirtualCollection { element in
            self.containsClosure(element) && contains(element)
        }
    }

}
