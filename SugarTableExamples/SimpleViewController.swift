//
//  SimpleViewController.swift
//  SugarTable
//
//  Created by Masahiko Tsujita on 2017/01/19.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import SugarTable
import enum Result.NoError
import ReactiveSwift

class TableDataSource<T>: TableDataSourceProtocol {
    typealias RowModel = T
    init(rows: [T]) {
        self.rows = rows
    }
    var rows: [T]
    var sectionCount: Int {
        return 1
    }
    func rowCount(ofSectionAt sectionIndex: Int) -> Int {
        return rows.count
    }
    var changes: SignalProducer<ChangeSet, NoError> { return .never }

    func context(forRowAt indexPath: IndexPath) -> Any {
        return rows[indexPath.row]
    }
    func context(forSectionAt sectionIndex: Int) -> Any {
        return rows
    }

    let virtualRows = VirtualCollection<(String, IndexPath)>()
}

class SimpleViewController: UITableViewController {

    let tableModel = TableDataSource(rows: [
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
        "Ten"
    ])

    let (lifetime, token) = Lifetime.make()

    override func viewDidLoad() {
        super.viewDidLoad()
        let disposable = tableView.sugarTable.bind(tableModel) { target, tableModel in
            target.cells(as: UITableViewCell.self, id: "Cell")
                .bind(
                    with: tableModel.virtualRows,
                    configuration: { cell, model, _, _ in
                        cell.textLabel!.text = model
                    },
                    height: .automatic(estimated: 44),
                    editing: { model, indexPath, tableView in
                        return .delete
                    }
                ) { binding in
                    self.didSelect <~ binding.didSelect
                    self.didDeselect <~ binding.didDeselect
                    self.commit <~ binding.commit
                }
        }
        lifetime.observeEnded {
            disposable.dispose()
        }
    }

    lazy var didSelect: Action<(UITableViewCell?, String, IndexPath, UITableView), Void, NoError> = {
        return Action { cell, text, indexPath, tableView in
            print("Did select text=\(text), at=\(indexPath)")
            return .empty
        }
    }()

    lazy var didDeselect: Action<(UITableViewCell?, String, IndexPath, UITableView), Void, NoError> = {
        return Action { cell, text, indexPath, tableView in
            print("Did deselect text=\(text), at=\(indexPath)")
            return .empty
        }
    }()

    lazy var commit: Action<(String, UITableViewCellEditingStyle, IndexPath, UITableView), Void, NoError> = {
        return Action { text, editingStyle, indexPath, tableView in
            self.tableModel.rows.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            return .empty
        }
    }()

}
